# T.A. Resources

A collection of resources and experiences I've gathered during my various teaching assistance opportunities. 

#### Design and Analysis of Algorithms

* Presented during spring of 2020 and fall of 2021 in Shiraz University.
* Instructed by [Koorush Ziarati](http://ziarati.net/).
* During my 8th and 9th semester, respectively.


#### Linear Algebra

* Presented during spring of 2020 in Shiraz University.
* Instructed by [Mohammad Taheri](https://scholar.google.com/citations?user=67GnXEsAAAAJ&hl=en).
* During my 8th semester.


#### Design and Implementation of Programming Languages

* Presented during fall of 2020 in Shiraz University.
* Instructed by [Morteza Keshtkaran](https://scholar.google.com/citations?user=3shbBG4AAAAJ).
* During my 7th semester.

#### Computer Architecture

* Presented during spring of 2019 in Shiraz University
* Instructed by [Farshad Khunjush](https://scholar.google.com/citations?user=nowZiqgAAAAJ).
* During my 6th semester.
<!-- * Instructed by [Farshad Khunjush](http://home.shirazu.ac.ir/~khunjush/). -->

---

##### TODO

<!-- #### Discrete Mathematics -->
###### Discrete Mathematics

* Presented during fall of 2018 in Shiraz University
* Instructed by [Morteza Keshtkaran](https://scholar.google.com/citations?user=3shbBG4AAAAJ).
* During my 3rd semester.
