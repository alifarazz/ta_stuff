// source: https://open.kattis.com/problems/ingestion

#include <algorithm>
#include <array>
#include <cassert>
#include <cinttypes>
#include <cstdint>
#include <cstdio>

const int MAXN = 100;
const int MAXM = 20'001;

using TypeCalories = std::int32_t;  // don't forget to chagne printf format string accordingly

std::array<std::array<TypeCalories, MAXN>, MAXM> dp;  // dp[MAXM][MAXN]
std::array<int, MAXN> course;                         // item[MAXN]
int M, N;

auto f(int m, int n, int prev_m)
{
  // Handle all the ways in which f(m, n, prev_m) could be 0. Then if dp[m][n] is 0, it means that it is not calculated
  // yet.
  if (n >= N or m == 0)  // no more courses left to eat OR cannot eat at all
    return TypeCalories{0};

  // cuz I'm paranoid
  assert(n < MAXN);
  assert(m <= MAXM);

  auto& u = dp[m][n];
  if (u != 0)  // u cannot be 0. if it is, then it's not yet computed
    return u;

  return u = std::max({
             f(prev_m, n + 1, prev_m),                           // don't eat current
             f((2 * m) / 3, n + 1, m) + std::min(m, course[n]),  // eat current
             f(M, n + 2, prev_m),                                // don't eat current and the next one
         });
}

int main()
{
  scanf("%i%i", &N, &M);
  for (int n = 0; n < N; n++)
    scanf("%i", course.data() + n);

  //////////////////////////////////////////////////////////
  // Commented out for the same reason stated at line 21. //
  //////////////////////////////////////////////////////////
  // for (int m = 1; m <= M; m++)
  //   for (int n = 0; n < N; n++)
  //     dp[m][n] = -1;
  // // for (int n = 0; n < N; n++)  dp[0][n] = 0; // reduandant
  // dp[0][0] = 0;

  printf("%" PRId32 "\n", f(M, 0, M));
}
