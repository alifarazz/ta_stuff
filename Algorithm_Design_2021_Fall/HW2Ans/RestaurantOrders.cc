// source: https://open.kattis.com/problems/orders

#include <algorithm>
#include <array>
#include <cstdio>
#include <vector>

const int MAXM = 1001;    // number of testcases
const int MAXN = 101;     // item count
const int MAXS = 30'001;  // total cost

enum dpState : int8_t { Ambiguous = -2, Impossible, Unset, Unique };

std::array<std::array<dpState, MAXN>, MAXS> dp;  // dp[MAXS][MAXN]
std::array<int, MAXN> item;                      // item[MAXN]
std::vector<int> solution;
int N, M, S;

inline auto calcUniqueSolution(int s, int n)
{
  while (not(s == 0 and n == 0)) {
    for (int i = 0, amnt = 0; i <= s / item[n]; i++, amnt += item[n]) {
      const auto sub_problem = dp[s - amnt][n - 1];

      if (sub_problem == Unique) {
        solution.insert(std::end(solution), i, n);
        s = s - amnt;
        n = n - 1;
        break;
      }
    }
  }
}

inline auto printSolution(int res_f)
{
  if (res_f == Ambiguous)
    puts("Ambiguous");
  else if (res_f == Impossible)
    puts("Impossible");
  else  // if (res_f == Unique)
  {
    calcUniqueSolution(S, N);
    std::reverse(std::begin(solution), std::end(solution));
    for (auto u : solution)
      printf("%i ", u);
    puts("");
  }
  // else
  // puts("Unset");
}

auto f(int s, int n)
{
  auto &u = dp[s][n];
  if (u != Unset)
    return u;

  for (int i = 0, amnt = 0; i <= s / item[n]; i++, amnt += item[n]) {
    const auto sub_problem = f(s - amnt, n - 1);

    if (sub_problem == Ambiguous)
      return u = Ambiguous;
    else if (sub_problem == Unique) {
      if (u == Unique)
        return u = Ambiguous;
      else {
        u = Unique;
      }
    }
  }
  if (u == Unset)
    u = Impossible;

  return u;
}

inline auto initializeDPtable(int S, int N)
{
  //////////////////////////////////////////////////////////////////
  // dp is in static memory so it's initizalized to 0 by default  //
  // and dpState::Unset has 0 as its int value.                   //
  // Hence, below statements are redundant and are commented out. //
  //////////////////////////////////////////////////////////////////
  // for (int s = 0; s <= S; s++)
  //   for (int n = 1; n <= N; n++)
  //     dp[s][n] = Unset;

  dp[0][0] = Unique;
  for (int s = 1; s <= S; s++)
    dp[s][0] = Impossible;
}

int main()
{
  scanf("%i", &N);

  /// indexing starts from 1 because I hate myself
  for (int i = 1; i <= N; i++)
    scanf("%i", item.data() + i);

  initializeDPtable(MAXS - 1, N);

  scanf("%i", &M);
  for (int m = 0; m < M; m++) {
    solution.clear();

    scanf("%i", &S);
    printSolution(f(S, N));
  }
}
