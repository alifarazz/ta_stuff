#!/usr/bin/env bash

echo "Assuming you have INKSCAPE installed..."
pdflatex -synctex=1  --shell-escape -interaction=nonstopmode "hw1".tex
