import strscans
import deques
import arraymancer

let
  MAXN = 1000
  MAXM = 1000

var grid = newTensor[int]([MAXN, MAXM])

func bfs(n, m: int, grid: var Tensor[int], queue: var Deque[(int, int)]): int =
  var time = 1
  var ended: bool
  while queue.len != 0:
    let (i, j) = queue.popFirst
    if i == j and j == -1:
      if ended:
        break
      else:
        inc time
        queue.addLast (-1, -1)
        ended = true
        continue
    for di in -1..1:
      for dj in -1..1:
        if not (i == j and j == 0):
          let
            ii = i + di
            jj = j + dj
          if 0 <= ii and 0 <= jj and i < n and j < m and grid[ii, jj] == -1:
            grid[ii, jj] = time
            ended = false
            queue.addLast (ii, jj)
  return time - 1

proc f(n, m: int) =
  var queue: Deque[(int, int)]
  for i in 0..<n:
    let line = stdin.readLine
    for j in 0..<m:
      let val = -ord(line[j] != 'f')
      grid[i, j] = val
      if val == 0:
        queue.addLast (i, j)
  if queue.len == 0: # no fire
    echo "0 0"
    return
  queue.addLast (-1, -1)
  let lastTime = bfs(n, m, grid, queue)
  block findIdx:
    for i in 0..<n:
      for j in 0..<m:
        if grid[i, j] == lastTime:
          echo i, ' ', j
          break findIdx

when isMainModule:
  var m, n, k: int
  while scanf(stdin.readLine, "$i $i $i", n, m, k) and m != 0 and n != 0 and k != 0:
    f(n, m)
