/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  http://www.gnu.org/licenses/gpl-3.0.html
 */
#include <algorithm>
#include <cstring>
#include <iostream>
#include <utility>
#include <vector>
using namespace std;

using Edge = pair<int, pair<int, int>>; // u --e--> v, <Weight of e, <u, v>>

constexpr int MAXN = 1e4 + 5;

vector<Edge> edges;
int n, m, mcstSize, mcstCost, rankk[MAXN], par[MAXN];
int costAirPort;

int Find(int u)
{
  if (par[u] == u)
    return u;
  return par[u] = Find(par[u]);
}

void Union(int u, int v)
{
  int uu = Find(u), vv = Find(v);
  if (rankk[uu] == rankk[vv]) {
    ++rankk[uu];
    par[uu] = vv;
  } else if (rankk[uu] < rankk[vv])
    par[vv] = uu;
  else
    par[uu] = vv;
}

void Kruskal()
{
  std::sort(&edges[0], &edges[m - 1] + 1);  // jesus
  for (int i = 0; i < m; i++) {
    auto u = edges[i].second.first, v = edges[i].second.second, w = edges[i].first;

    if (w < costAirPort && Find(u) != Find(v)) {
      Union(u, v);
      mcstCost += edges[i].first;
      mcstCost -= costAirPort;
      ++mcstSize;
    }
  }
}

int mainmain()
{
  int i;
  cin >> n >> m >> costAirPort;
  mcstCost = costAirPort * n;
  edges.reserve(m);
  for (i = 0; i < m; i++) {
    cin >> edges[i].second.first >> edges[i].second.second >> edges[i].first;
    edges[i].second.first--, edges[i].second.second--;
  }

  for (i = 0; i < n; i++)
    par[i] = i;
  Kruskal();

  cout << mcstCost << ' ' << n - mcstSize << endl;
  return 0;
}

int main()
{
  ios::sync_with_stdio(false);
  cin.tie(nullptr);

  int i;
  cin >> i;
  for (int j = 1; j <= i; j++) {
    cout << "Case " << j << ": ";
    mainmain();

    mcstSize = 0;

    std::fill(rankk, rankk + n, 0);
    edges.clear();
  }
}
